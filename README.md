# README #

### Database Design:
* technical_design/database.mwb
* Design Tool: MYSQL Workbench


### API Design:
* technical_design/Customer.v1.yaml
* Design Tool: Stoplight

### Technical Stack:
* MySQL as database.
* Application is written in Golang.
* Use GIN as Web Framework.
* Use GORM as ORM library.
* Use MySQL FULL-TEXT SEARCH.

### Setup:
* docker-compose up --build
* Database is exposed at port 3307. (Username: mysql, Password: longto)
* Service will be up and running at port 8080. 2 APIS:
 	* POST: localhost:8080/api/customer/create/
 	* POST: localhost:8080/api/customer/search/
