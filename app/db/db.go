package db

import (
	"app/settings"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
	_ "github.com/mattn/go-sqlite3"
	"github.com/sirupsen/logrus"
	"time"
)

var log = settings.GetLogger("main")

var db *gorm.DB

func init() {
	var err error
	db, err = gorm.Open(settings.DbDialect, settings.DbConnectionString)

	if err != nil {
		var fields = logrus.Fields{
			"dialect":           settings.DbDialect,
			"connection_string": settings.DbConnectionString,
		}

		log.WithFields(fields).WithError(err).Error("Error when init db")

		time.Sleep(5 * time.Second) // Sleep for waiting send to sentry complete

		panic(err)
	}
	db.LogMode(settings.SqlDebug)
	// SetMaxIdleConns sets the maximum number of connections in the idle connection pool.
	db.DB().SetMaxIdleConns(2)
	// SetMaxOpenConns sets the maximum number of open connections to the database.
	db.DB().SetMaxOpenConns(10)
	// SetConnMaxLifetime sets the maximum amount of time a connection may be reused.
	db.DB().SetConnMaxLifetime(30 * time.Minute)
}

func GetDB(name ...string) *gorm.DB {
	if len(name) == 0 {
		return db
	}

	return db
}
