package services

import (
	"app/models"
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"net/http"
	"strconv"
)

type CustomerService struct{}

type CreateCustomerReq struct {
	Name  string `json:"name" binding:"required"`
	Phone string `json:"phone" binding:"required"`
}

func (s *CustomerService) CreateCustomer(c *gin.Context) {
	var r CreateCustomerReq
	if err := c.ShouldBindJSON(&r); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	if len(r.Name) > 255 {
		c.JSON(http.StatusBadRequest, gin.H{"error": "name can not > 255 characters"})
		return
	}
	if len(r.Phone) != 11 {
		c.JSON(http.StatusBadRequest, gin.H{"error": "phone must have 11 digits"})
		return
	}
	if _, err := strconv.Atoi(r.Phone); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "invalid phone number"})
		return
	}
	cus := models.Customer{
		Name:  r.Name,
		Phone: r.Phone,
	}

	if err := customerModelImpl.Create(&cus); err != nil {
		var fields = logrus.Fields{
			"customer": cus,
		}

		log.WithFields(fields).WithError(err).Error("failed to creat customer record")
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}
	c.JSON(http.StatusOK, gin.H{"data": cus})
}

type SearchCustomerReq struct {
	Prefix string `json:"prefix" binding:"required"`
	Limit  uint32 `json:"limit"`
	Offset uint32 `json:"offset"`
}

func (s *CustomerService) SearchCustomers(c *gin.Context) {
	var r SearchCustomerReq
	if err := c.ShouldBindJSON(&r); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	if len(r.Prefix) < 1 || len(r.Prefix) > 11 {
		c.JSON(http.StatusBadRequest, gin.H{"error": "prefix length range from 1 to 11"})
		return
	}
	if r.Limit == 0 {
		r.Limit = 50
	}

	cus, count, err := customerModelImpl.GetAllWithPaging(
		r.Offset,
		r.Limit,
		"prefix",
		fmt.Sprintf("%v*",r.Prefix),
	)
	if err != nil {
		var fields = logrus.Fields{
			"request": r,
		}

		log.WithFields(fields).WithError(err).Error("failed to search customer record")
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}
	c.JSON(http.StatusOK, gin.H{"offset": r.Offset, "total": count, "limit": r.Limit, "data": cus})
}
