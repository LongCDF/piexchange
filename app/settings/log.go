package settings

import (
	"github.com/lestrrat-go/file-rotatelogs"
	"github.com/rifflock/lfshook"
	"github.com/sirupsen/logrus"
	"io/ioutil"
	"os"
	"time"
)

var emptyLogger = logrus.New()
var mainLogger *logrus.Logger
var rotateHook *lfshook.LfsHook

func GetLogger(name string) *logrus.Logger {
	switch name {
	case "main":
		return mainLogger
	default:
		return emptyLogger
	}

}

func initLog() {
	rotateHook = createRotateHook(LogPath)

	mainLogger = createMainLogger()
}

func createMainLogger() *logrus.Logger {
	var log = createLog()

	log.SetLevel(logrus.InfoLevel)

	return log
}

func createLog() *logrus.Logger {
	var log = logrus.New()

	if Env == LiveEnv {
		log.SetOutput(ioutil.Discard)
	} else {
		log.SetFormatter(&logrus.JSONFormatter{})
		log.SetOutput(os.Stdout)
	}

	log.Hooks.Add(rotateHook)

	return log
}

func createRotateHook(file string) *lfshook.LfsHook {
	var writer, err = rotatelogs.New(
		file+".%Y-%m-%d",
		rotatelogs.WithLinkName(file),
		rotatelogs.WithMaxAge(15*time.Duration(24)*time.Hour),
		rotatelogs.WithRotationTime(24*time.Hour),
	)

	if err != nil {
		panic(err)
	}

	return lfshook.NewHook(
		lfshook.WriterMap{
			logrus.DebugLevel: writer,
			logrus.TraceLevel: writer,
			logrus.InfoLevel:  writer,
			logrus.WarnLevel:  writer,
			logrus.ErrorLevel: writer,
			logrus.FatalLevel: writer,
		},
		&logrus.JSONFormatter{},
	)
}
