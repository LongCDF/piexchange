package settings

import (
	"errors"
	"fmt"
	"github.com/spf13/viper"
)

type Environment string

const (
	LiveEnv          Environment = "LIVE"
	TestEnv          Environment = "TEST"
	UatEnv           Environment = "UAT"
	DevEnv           Environment = "DEV"
	connectionString             = "%s:%s@%s(%s)/%s?charset=utf8mb4&parseTime=True&loc=Local"
)

var (
	Env = LiveEnv

	DbDialect          = ""
	DbConnectionString = ""
	DbHost             = ""
	DbUnixSocketPath   = ""
	DbName             = ""
	DbUser             = ""
	DbPassword         = ""
	LogPath            = ""
	SqlDebug           = true
)

func init() {
	setDefaultEnvVariables()
	loadEnvVariables()
	initLog()

	switch DbDialect {
	case "mysql":
		var address = DbUnixSocketPath
		var protocol = "unix"
		if address == "" {
			address = DbHost
			protocol = "tcp"
		}

		DbConnectionString = fmt.Sprintf(connectionString, DbUser, DbPassword, protocol, address, DbName)
	case "sqlite3":
		DbConnectionString = DbName
	default:
		panic(errors.New("gorm dialect not support"))
	}
}

func setDefaultEnvVariables() {
	viper.SetDefault("SQL_DEBUG", false)
}

func loadEnvVariables() {
	viper.AutomaticEnv()

	Env = Environment(viper.GetString("ENV"))

	DbHost = viper.GetString("DB_HOST")
	DbName = viper.GetString("DB_NAME")
	DbUser = viper.GetString("DB_USER")
	DbPassword = viper.GetString("DB_PASS")
	DbDialect = viper.GetString("DB_DIALECT")

	SqlDebug = viper.GetBool("SQL_DEBUG")
	LogPath = viper.GetString("LOG_PATH")
}
