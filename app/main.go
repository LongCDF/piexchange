package main

import (
	"app/services"
	"github.com/gin-gonic/gin"
	"log"
)

func main() {
	r := gin.Default()
	s := services.CustomerService{}
	a := r.Group("/api/customer/")
	{
		a.POST("/create/", s.CreateCustomer)
		a.POST("/search/", s.SearchCustomers)
	}

	// Run server
	if err := r.Run(); err != nil {
		log.Fatal(err)
	}
}
