package models

import (
	"app/db"
	"database/sql"
	"github.com/jinzhu/gorm"
	"time"
)

type CustomerStatus string

const (
	Active      CustomerStatus = "A"
	Deactivated                = "D"
)

type Customer struct {
	ID        uint64    `gorm:"AUTO_INCREMENT;NOT NULL;primary_key;" sql:"bigint unsigned;NOT NULL;primary_key;"`
	CreatedAt time.Time `gorm:"not null; INDEX:idx_created_at; type:timestamp; default:CURRENT_TIMESTAMP"`
	UpdatedAt time.Time `gorm:"not null; type:timestamp; default:CURRENT_TIMESTAMP"`
	Name      string    `gorm:"not null; size:255; INDEX:idx_customer_name;"`
	Phone     string    `gorm:"not null; size:11; unique_index"`
	Status    string    `gorm:"not null; size:1; default:'A'"`
}

type CustomerModelImpl struct {
	db *gorm.DB
}

func (m *CustomerModelImpl) Create(s *Customer) error {
	return db.GetDB().Create(&s).Error
}

func (m *CustomerModelImpl) GetAllWithPaging(offset uint32, limit uint32, conditions ...interface{}) ([]Customer, uint32, error) {
	var results = make([]Customer, 0, 0)
	var count uint32
	var err error

	if err = checkParams(conditions); err != nil {
		return nil, count, err
	}

	if count, err = m.count(conditions...); err != nil {
		return nil, count, err
	}

	conditions = addParams(conditions, "offset", offset, "limit", limit)

	switch err := m.buildSelectQuery(conditions...).Find(&results).Error; err {
	case sql.ErrNoRows:
		return results, count, nil
	case nil:
		return results, count, nil
	default:
		return results, count, err
	}
}

func (m *CustomerModelImpl) count(conditions ...interface{}) (uint32, error) {
	var count uint32
	var err error

	err = m.buildSelectQuery(conditions...).Model(&Customer{}).Count(&count).Error

	return count, err
}

func (*CustomerModelImpl) buildSelectQuery(conditions ...interface{}) *gorm.DB {
	var conditionMap = buildParams(conditions)
	var querySet = db.GetDB().Model(&Customer{})

	if name, ok := conditionMap["name"]; ok && !isNil(name) {
		querySet = querySet.Where("name = ?", name)
	}

	if status, ok := conditionMap["status"]; ok && !isNil(status) {
		querySet = querySet.Where("status = ?", status)
	}

	if prefix, ok := conditionMap["prefix"]; ok && !isNil(prefix) {
		querySet = querySet.Where("MATCH (phone) AGAINST (? IN BOOLEAN MODE)", prefix)
	}

	if offset, ok := conditionMap["offset"]; ok && !isNil(offset) {
		querySet = querySet.Offset(offset)
	}

	if limit, ok := conditionMap["limit"]; ok && !isNil(limit) {
		querySet = querySet.Limit(limit)
	}

	return querySet
}
