package models

import "errors"

var (
	ConditionInvalid = errors.New("conditions params must be pairs")
	Invalid          = errors.New("page and page_size must be in conditions")
)
