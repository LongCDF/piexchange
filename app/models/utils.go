package models

import (
	"reflect"
)

func buildParams(conditions []interface{}) map[string]interface{} {
	var conditionMap = make(map[string]interface{})

	for i := 0; i < len(conditions); i = i + 2 {
		conditionMap[conditions[i].(string)] = conditions[i+1]
	}

	return conditionMap
}

func checkParams(conditions []interface{}) error {
	if len(conditions)%2 != 0 {
		return ConditionInvalid
	}

	return nil
}

func addParams(conditions []interface{}, extraConditions ...interface{}) []interface{} {
	return append(conditions, extraConditions...)
}

func isNil(v interface{}) bool {
	return v == nil || (reflect.ValueOf(v).Kind() == reflect.Ptr && reflect.ValueOf(v).IsNil())
}
