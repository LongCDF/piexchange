package models

import "app/db"

func init() {
	db.GetDB().AutoMigrate(Customer{})
	db.GetDB().Exec("ALTER TABLE `customers` ADD FULLTEXT INDEX `index_customer_on_phone` (`phone`);")
}
